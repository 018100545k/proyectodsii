﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SistemaAcademicoUniversidad.Hijo1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .auto-style1 {
        width: 300px;
        height: 93px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p class="MsoNormal">
    <span style="font-size:12.0pt;line-height:107%">La Escuela Profesional de Ingeniería de Sistemas tiene más de 26 años de experiencia formando ingenieros capaces de diseñar soluciones para una amplia gama de problemas en el sector de la ingeniería y de otras disciplinas, mediante la aplicación del pensamiento sistémico y el uso de las tecnologías de la información y comunicación.<o:p></o:p></span></p>
    <p class="MsoNormal">
    <span style="font-size:12.0pt;line-height:107%">El graduado de la Escuela Profesional de Ingeniería de Sistemas está preparado para analizar, comprender, evaluar, seleccionar y optimizar, como un todo integrado, el funcionamiento de la estructura organizacional en una empresa. Asimismo, está capacitado para transformar una necesidad operativa en un sistema diseñado para satisfacer esa necesidad, a través de procesos iterativos de definición de sistemas, de síntesis del problema y de análisis, diseño y evaluación de las soluciones.<o:p></o:p></span></p>
    <p class="MsoNormal">
    <span style="font-size:12.0pt;line-height:107%">El ingeniero de sistemas de la Universidad Andina del Cusco domina las tecnologías de información, tecnologías de comunicación, modelación y simulación de sistemas. Además, conoce normas y estándares de calidad para la gestión de sistemas de información, gestión de TI, seguridad informática, ciberseguridad, desarrollo de software, entre otras. De igual forma, conceptúa y maneja la teoría de investigación, aplica métodos, técnicas e instrumentos de investigación científica.<o:p></o:p></span></p>
    <p><span style="color: rgb(44, 44, 44); font-family: Muli; font-size: 24.0pt; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 900; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Desempeño laboral</span></p>
    <p class="MsoNormal">
    El Ingeniero de Sistemas se desempeña de manera solvente en todos los sectores económicos; en organizaciones de tipo privado, público o mixto; en el entorno regional, nacional, e internacional; en aquellas áreas donde sea necesaria la aplicación eficiente o el desarrollo especializado de tecnologías de información y comunicación para una adecuada gestión de la información. Además, su espíritu emprendedor e innovador posibilita que pueda generar empresas y productos tecnológicos. Lidera y se integra en equipos multidisciplinarios, gerencia proyectos de desarrollo y mantenimiento de software.<o:p></o:p></p>
    <p><span style="color: rgb(44, 44, 44); font-family: Muli; font-size: 24.0pt; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 900; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">Acreditación</span></p>
    <p class="MsoNormal">
    El programa de Ingeniería de Sistemas de la Universidad Andina del Cusco está acreditado por el Comité de Acreditación de Computación de ICACIT<o:p></o:p></p>
    <p class="MsoNormal">
        <a href="http://www.icacit.org.pe">http://www.icacit.org.pe</a><o:p></o:p></p>
    <p>
        <a href="http://www.icacit.org.pe">
    <img alt="" class="auto-style1" src="Images/icacit.jpg" /></a></p>
<span style="color: rgb(94, 106, 120); font-family: &quot;Open Sans&quot;; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: -0.1px; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">
<span style="color: rgb(44, 44, 44); font-family: Muli; font-size: 32px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 900; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(245, 247, 248); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">
<div id="iguru_message_604f50bc1ef3b" class="iguru_module_message_box type_custom" style="box-sizing: border-box; vertical-align: baseline; margin: 0px 0px 15px; border: 0px; padding: 12px 12px 12px 85px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; position: relative; z-index: 0; min-height: 73px; border-radius: 5px; box-shadow: rgba(0, 0, 0, 0.08) 6px 5px 25px; background-color: rgb(255, 255, 255); color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
    <div class="message_icon_wrap" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: absolute; z-index: 1; top: 5px; left: 5px; width: 63px; height: calc(100% - 10px); border-radius: 5px; background-color: rgb(0, 206, 254);">
        <br class="Apple-interchange-newline" />
        <i class="message_icon fa fa-envelope-open" style="box-sizing: border-box; font: 35px / 1 FontAwesome; display: flex; text-rendering: auto; -webkit-font-smoothing: antialiased; -webkit-box-pack: center; justify-content: center; -webkit-box-align: center; align-items: center; height: 63px; text-align: center; color: rgb(255, 255, 255);"></i>
    </div>
    <div class="message_content" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
        <h4 class="message_title" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: Muli; font-size: 18px; font-weight: 700; font-style: inherit; color: rgb(0, 206, 254); line-height: 1.25;">INFORMES:</h4>
        <div class="message_text" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 8px 0px 0px; outline: 0px; font-family: inherit; font-size: 14px; font-weight: inherit; font-style: inherit; line-height: 1.25; color: rgb(95, 95, 95);">
            ep_sistemas@uandina.edu.pe</div>
    </div>
</div>
<div id="iguru_message_604f50bc1ef95" class="iguru_module_message_box type_custom" style="box-sizing: border-box; vertical-align: baseline; margin: 0px 0px 15px; border: 0px; padding: 12px 12px 12px 85px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; position: relative; z-index: 0; min-height: 73px; border-radius: 5px; box-shadow: rgba(0, 0, 0, 0.08) 6px 5px 25px; background-color: rgb(255, 255, 255); color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
    <div class="message_icon_wrap" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: absolute; z-index: 1; top: 5px; left: 5px; width: 63px; height: calc(100% - 10px); border-radius: 5px; background-color: rgb(0, 206, 254);">
        <i class="message_icon fa fa-phone" style="box-sizing: border-box; font: 35px / 1 FontAwesome; display: flex; text-rendering: auto; -webkit-font-smoothing: antialiased; -webkit-box-pack: center; justify-content: center; -webkit-box-align: center; align-items: center; height: 63px; text-align: center; color: rgb(255, 255, 255);"></i>
    </div>
    <div class="message_content" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
        <h4 class="message_title" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: Muli; font-size: 18px; font-weight: 700; font-style: inherit; color: rgb(0, 206, 254); line-height: 1.25;">CONTACTO:</h4>
        <div class="message_text" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 8px 0px 0px; outline: 0px; font-family: inherit; font-size: 14px; font-weight: inherit; font-style: inherit; line-height: 1.25; color: rgb(95, 95, 95);">
            084 - 60 5000 / ANEXO: 1404</div>
    </div>
</div>
</span></span>
</asp:Content>
