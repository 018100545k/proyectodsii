﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Docentes.aspx.cs" Inherits="SistemaAcademicoUniversidad.Docentes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
        &nbsp;</p>
    <div class="wpb_column vc_column_container vc_col-sm-6" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; width: 600px; position: relative; min-height: 1px; float: left;">
        <div class="vc_column-inner" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 15px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; width: 600px;">
            <div class="wpb_wrapper" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                <div id="iguru_dlh_604f5c646635d" class="iguru_module_double_headings aleft " style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: Muli; font-size: 16px; font-weight: inherit; font-style: inherit; text-align: left;">
                    <div class="dlh_subtitle" style="box-sizing: border-box; vertical-align: baseline; margin: 0px 0px 12px; border: 0px; padding: 0px; outline: 0px; font-family: Quicksand; font-size: 16px; font-weight: 700; font-style: inherit; display: inline-block; position: relative; z-index: 1; color: rgb(0, 206, 254); line-height: 20px;">
                        <span style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: inherit; font-weight: inherit; font-style: inherit; line-height: inherit; color: inherit;">
                        <br class="Apple-interchange-newline" />
                        DIRECTOR</span></div>
                    <div class="dlh_title" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 32px; font-weight: 900; font-style: inherit; position: relative; z-index: 1; color: rgb(44, 44, 44); line-height: 1.563;">
                        <span style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: inherit; font-weight: inherit; font-style: inherit; line-height: inherit; color: inherit;">Escuela Profesional</span></div>
                </div>
                <div class="iguru_module_spacing" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                    <div class="spacing_size spacing_size-initial" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; height: 16px;">
                    </div>
                </div>
                <div class="wpb_text_column wpb_content_element " style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                    <div class="wpb_wrapper" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                        <p style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 18px; font-weight: inherit; font-style: inherit; color: rgb(94, 106, 120); letter-spacing: -0.1px;">
                            Dr. Ing. Lornel Antonio Rivas Mago</p>
                    </div>
                </div>
                <div class="iguru_module_spacing" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                    <div class="spacing_size spacing_size-initial" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; height: 23px;">
                    </div>
                </div>
                <div class="wpb_single_image wpb_content_element vc_align_left  vc_custom_1613195838334" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 20px !important; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; text-align: left;">
                    <figure class="wpb_wrapper vc_figure" style="box-sizing: border-box; border-radius: inherit; display: inline-block; vertical-align: top; margin: 0px; max-width: 100%;">
                        <a class="vc_single_image-wrapper vc_box_shadow_border_circle  vc_box_border_grey prettyphoto" data-rel="prettyPhoto[rel-4745-2472808397]" href="https://www.uandina.edu.pe/wp-content/uploads/2021/02/is-lormel-rivas.jpg" style="box-sizing: border-box; vertical-align: top; margin: 0px; border: none; padding: 6px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; transition: all 0.4s ease 0s; text-decoration: none; color: rgb(29, 192, 222); border-radius: 50%; display: inline-block; max-width: 100%; box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 5px; overflow: hidden;" target="_self">
                        <img alt="" class="vc_single_image-img attachment-medium" height="300" sizes="(max-width: 270px) 100vw, 270px" src="https://www.uandina.edu.pe/wp-content/uploads/2021/02/is-lormel-rivas-270x300.jpg" srcset="https://www.uandina.edu.pe/wp-content/uploads/2021/02/is-lormel-rivas-270x300.jpg 270w, https://www.uandina.edu.pe/wp-content/uploads/2021/02/is-lormel-rivas-768x853.jpg 768w, https://www.uandina.edu.pe/wp-content/uploads/2021/02/is-lormel-rivas.jpg 881w" style="box-sizing: border-box; vertical-align: top; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; max-width: 100%; height: auto; user-select: none; border-radius: 50%; box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 5px; overflow: hidden;" width="270" /></a></figure>
                </div>
                <div id="iguru_spacer_604f5c6466850" class="iguru_module_spacing responsive_active" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                    <div class="spacing_size spacing_size-initial" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; display: block; height: 30px;">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wpb_column vc_column_container vc_col-sm-6" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; width: 600px; position: relative; min-height: 1px; float: left;">
        <div class="vc_column-inner" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 15px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; width: 600px;">
            <div class="wpb_wrapper" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                <div id="iguru_spacer_604f5c64668ce" class="iguru_module_spacing responsive_active" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                    <div class="spacing_size spacing_size-initial" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; display: block; height: 10px;">
                    </div>
                </div>
                <div id="iguru_dlh_604f5c64668f3" class="iguru_module_double_headings aleft " style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: Muli; font-size: 16px; font-weight: 400; font-style: normal; text-align: left; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                    <div class="dlh_subtitle" style="box-sizing: border-box; vertical-align: baseline; margin: 0px 0px 12px; border: 0px; padding: 0px; outline: 0px; font-family: Quicksand; font-size: 16px; font-weight: 700; font-style: inherit; display: inline-block; position: relative; z-index: 1; color: rgb(0, 206, 254); line-height: 20px;">
                        <span style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: inherit; font-weight: inherit; font-style: inherit; line-height: inherit; color: inherit;">DIRECTORA</span></div>
                    <div class="dlh_title" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 32px; font-weight: 900; font-style: inherit; position: relative; z-index: 1; color: rgb(44, 44, 44); line-height: 1.563;">
                        <span style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: inherit; font-weight: inherit; font-style: inherit; line-height: inherit; color: inherit;">Departamento Académico</span></div>
                </div>
                <div class="iguru_module_spacing" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                    <div class="spacing_size spacing_size-initial" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; height: 16px;">
                    </div>
                </div>
                <div class="wpb_text_column wpb_content_element " style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                    <div class="wpb_wrapper" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                        <p style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 18px; font-weight: inherit; font-style: inherit; color: rgb(94, 106, 120); letter-spacing: -0.1px;">
                            Ing. Maria Isabel Acurio Gutierrez</p>
                    </div>
                </div>
                <div class="iguru_module_spacing" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                    <div class="spacing_size spacing_size-initial" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; height: 23px;">
                    </div>
                </div>
                <div class="wpb_single_image wpb_content_element vc_align_left  vc_custom_1613227254392" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 20px !important; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; text-align: left; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                    <figure class="wpb_wrapper vc_figure" style="box-sizing: border-box; border-radius: inherit; display: inline-block; vertical-align: top; margin: 0px; max-width: 100%;">
                        <a class="vc_single_image-wrapper vc_box_shadow_border_circle  vc_box_border_grey prettyphoto" data-rel="prettyPhoto[rel-4745-99094494]" href="https://www.uandina.edu.pe/wp-content/uploads/2021/02/is-isabel-acurio.jpg" style="box-sizing: border-box; vertical-align: top; margin: 0px; border: none; padding: 6px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; transition: all 0.4s ease 0s; text-decoration: none; color: rgb(29, 192, 222); border-radius: 50%; display: inline-block; max-width: 100%; box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 5px; overflow: hidden;" target="_self">
                        <img alt="" class="vc_single_image-img attachment-medium" height="300" sizes="(max-width: 270px) 100vw, 270px" src="https://www.uandina.edu.pe/wp-content/uploads/2021/02/is-isabel-acurio-270x300.jpg" srcset="https://www.uandina.edu.pe/wp-content/uploads/2021/02/is-isabel-acurio-270x300.jpg 270w, https://www.uandina.edu.pe/wp-content/uploads/2021/02/is-isabel-acurio-768x853.jpg 768w, https://www.uandina.edu.pe/wp-content/uploads/2021/02/is-isabel-acurio.jpg 881w" style="box-sizing: border-box; vertical-align: top; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; max-width: 100%; height: auto; user-select: none; border-radius: 50%; box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 5px; overflow: hidden;" width="270" /></a></figure>
                </div>
                <div id="iguru_spacer_604f5c6466db6" class="iguru_module_spacing responsive_active" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                    <br class="Apple-interchange-newline" />
                </div>
            </div>
        </div>
    </div>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
<p>
        &nbsp;</p>
<p>
        &nbsp;</p>
    <div class="dlh_subtitle" style="box-sizing: border-box; vertical-align: baseline; margin: 0px 0px 12px; border: 0px; padding: 0px; outline: 0px; font-family: Quicksand; font-size: 16px; font-weight: 700; font-style: normal; display: inline-block; position: relative; z-index: 1; color: rgb(0, 206, 254); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; line-height: 20px;">
        <span style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: inherit; font-weight: inherit; font-style: inherit; line-height: inherit; color: inherit;">ESCUELA PROFESIONAL DE INGENIERÍA DE SISTEMAS</span></div>
    <div class="dlh_title" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: Muli; font-size: 32px; font-weight: 900; font-style: normal; position: relative; z-index: 1; color: rgb(44, 44, 44); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; line-height: 1.563;">
        <span style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: inherit; font-weight: inherit; font-style: inherit; line-height: inherit; color: inherit;">Plana docente</span></div>
    <p>
        <img src="https://www.uandina.edu.pe/wp-content/uploads/2021/02/plana-docente-sistemas-1024x594.jpg" /></p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <div class="wpb_column vc_column_container vc_col-sm-4" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; width: 400px; position: relative; min-height: 1px; float: left;">
        <div class="vc_column-inner" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 15px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; width: 400px;">
            <div class="wpb_wrapper" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                <div class="wpb_text_column wpb_content_element " style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                    <div class="wpb_wrapper" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                        <ul class="iguru_dot secondary" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 20px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">ING. ACURIO GUTIERREZ, MARIA ISABEL</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">MG. ABE MIGUITA, PEDRO ENRIQUE</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">MTRO. ING.&nbsp; ALCCA ZELA, ERICK</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">ING.&nbsp; ARDILES ROMERO, VELIA</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">MTRO. ING. BERNALES GUZMAN, YESSENIA</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">MG. ING. CHAVEZ ESPINOZA, WILLIAM ALBERTO</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">MTRO. ING. CHOQUE SOTO, VANESSA MARIBEL</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">MG. ING. CARRASCO POBLETE, EDWIN</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">ING.&nbsp; CUBA DEL CASTILLO, MARIA YORNET</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">MG. ING. DE LA VEGA BELLIDO, VIVIAN LUZ</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">MG. ING.&nbsp; DEL CARPIO CUENTAS, LUIS ENRIQUE</li>
                        </ul>
                    </div>
                </div>
                <div class="iguru_module_spacing" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                    <div class="spacing_size spacing_size-initial" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; height: 30px;">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wpb_column vc_column_container vc_col-sm-4" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; width: 400px; position: relative; min-height: 1px; float: left;">
        <div class="vc_column-inner" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 15px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; width: 400px;">
            <div class="wpb_wrapper" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                <div id="iguru_spacer_604f5c6467724" class="iguru_module_spacing responsive_active" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                    <div class="spacing_size spacing_size-initial" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; display: block; height: 10px;">
                    </div>
                </div>
                <div class="wpb_text_column wpb_content_element " style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                    <div class="wpb_wrapper" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                        <ul class="iguru_dot secondary" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 20px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">ING.&nbsp; ESPETIA HUAMANGA, HUGO</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">MTRO. ING GAMARRA SALDIVAR, ENRIQUE</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">DR. ING. GANVINI VALCARCEL, CRISTHIAN EDUARDO</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">MG. ING. GONZALES CONDORI, HARRY YEISON</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">MTRO. ING. HOLGUIN HERRERA, MELISA BETYS</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">MTRO. ING. HUAMAN ATAULLUCO, FELIX ENRIQUE</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">ING.&nbsp; LEON NUÑEZ, LIDA</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">MG. ING. MARCA AIMA, MONICA</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">ING.&nbsp; MOLERO DELGADO, IVAN</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">ING. MOREANO CORDOVA, JAVIER</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">ING. MUÑOZ YEPEZ, JOSE LUIS</li>
                        </ul>
                    </div>
                </div>
                <div class="iguru_module_spacing" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                    <div class="spacing_size spacing_size-initial" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; height: 30px;">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wpb_column vc_column_container vc_col-sm-4" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; width: 400px; position: relative; min-height: 1px; float: left;">
        <div class="vc_column-inner" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 15px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; width: 400px;">
            <div class="wpb_wrapper" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                <div id="iguru_spacer_604f5c646784d" class="iguru_module_spacing responsive_active" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                    <div class="spacing_size spacing_size-initial" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; display: block; height: 10px;">
                    </div>
                </div>
                <div class="wpb_text_column wpb_content_element " style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                    <div class="wpb_wrapper" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                        <ul class="iguru_dot secondary" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 20px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">DRA. ING.&nbsp; NUÑEZ PACHECO, MARUJA</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">MG. ING. PALOMINO CAHUAYA, ARIADNA</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">DR. ING. PALOMINO OLIVERA, EMILIO</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">ING.&nbsp; POCCORI UMERES, GODOFREDO</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">ING.&nbsp; RAMIREZ VARGAS, ADRIEL</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">DR. ING.&nbsp; RIVAS MAGO, LORNEL ANTONIO</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">MTRO. ING.&nbsp; SOTA ORELLANA, LUIS ALBERTO</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">MTRA. ING. VARGAS VERA, LIZET</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">ING. VILLENA LEON, OLMER CLAUDIO</li>
                            <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">MTRO. ING.&nbsp; ZAMBRANO INCHAUSTEGUI, CARLOS ALBERTO</li>
                        </ul>
                    </div>
                </div>
                <div class="iguru_module_spacing" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                    <br class="Apple-interchange-newline" />
                </div>
            </div>
        </div>
    </div>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <div id="iguru_message_604f5c6469415" class="iguru_module_message_box type_custom" style="box-sizing: border-box; vertical-align: baseline; margin: 0px 0px 15px; border: 0px; padding: 12px 12px 12px 85px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; position: relative; z-index: 0; min-height: 73px; border-radius: 5px; box-shadow: rgba(0, 0, 0, 0.08) 6px 5px 25px; background-color: rgb(255, 255, 255); color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        <div class="message_icon_wrap" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: absolute; z-index: 1; top: 5px; left: 5px; width: 63px; height: calc(100% - 10px); border-radius: 5px; background-color: rgb(0, 206, 254);">
            <br class="Apple-interchange-newline" />
            <i class="message_icon fa fa-envelope-open" style="box-sizing: border-box; font: 35px / 1 FontAwesome; display: flex; text-rendering: auto; -webkit-font-smoothing: antialiased; -webkit-box-pack: center; justify-content: center; -webkit-box-align: center; align-items: center; height: 63px; text-align: center; color: rgb(255, 255, 255);"></i>
        </div>
        <div class="message_content" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
            <h4 class="message_title" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: Muli; font-size: 18px; font-weight: 700; font-style: inherit; color: rgb(0, 206, 254); line-height: 1.25; text-align: left;">INFORMES:</h4>
            <div class="message_text" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 8px 0px 0px; outline: 0px; font-family: inherit; font-size: 14px; font-weight: inherit; font-style: inherit; line-height: 1.25; color: rgb(95, 95, 95);">
                ep_sistemas@uandina.edu.pe</div>
        </div>
    </div>
    <div id="iguru_message_604f5c646949c" class="iguru_module_message_box type_custom" style="box-sizing: border-box; vertical-align: baseline; margin: 0px 0px 15px; border: 0px; padding: 12px 12px 12px 85px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; position: relative; z-index: 0; min-height: 73px; border-radius: 5px; box-shadow: rgba(0, 0, 0, 0.08) 6px 5px 25px; background-color: rgb(255, 255, 255); color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        <div class="message_icon_wrap" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: absolute; z-index: 1; top: 5px; left: 5px; width: 63px; height: calc(100% - 10px); border-radius: 5px; background-color: rgb(0, 206, 254);">
            <i class="message_icon fa fa-phone" style="box-sizing: border-box; font: 35px / 1 FontAwesome; display: flex; text-rendering: auto; -webkit-font-smoothing: antialiased; -webkit-box-pack: center; justify-content: center; -webkit-box-align: center; align-items: center; height: 63px; text-align: center; color: rgb(255, 255, 255);"></i>
        </div>
        <div class="message_content" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
            <h4 class="message_title" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: Muli; font-size: 18px; font-weight: 700; font-style: inherit; color: rgb(0, 206, 254); line-height: 1.25;">CONTACTO:</h4>
            <div class="message_text" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 8px 0px 0px; outline: 0px; font-family: inherit; font-size: 14px; font-weight: inherit; font-style: inherit; line-height: 1.25; color: rgb(95, 95, 95);">
                084 - 60 5000 / ANEXO: 1404</div>
        </div>
    </div>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
</asp:Content>
