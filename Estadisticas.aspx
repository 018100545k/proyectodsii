﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Estadisticas.aspx.cs" Inherits="SistemaAcademicoUniversidad.Estadisticas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 600px;
        }
        .auto-style3 {
            width: 500px;
        }
        .auto-style4 {
            width: 600px;
            height: 23px;
        }
        .auto-style5 {
            width: 500px;
            height: 23px;
        }
        .auto-style6 {
            height: 23px;
        }
        .auto-style7 {
            margin-top: 0px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p style="font-size: x-large; color: #00FFFF; font-weight: bold;">
        <br />
        AÑO 2020</p>
    <table class="auto-style1" style="border-style: groove; text-align: center;">
        <tr>
            <td class="auto-style4" style="background-color: #CCCCCC; font-weight: bold;">SEMESTRE ACADÉMICO</td>
            <td class="auto-style4" style="background-color: #CCCCCC; font-weight: bold;">N° ALUMNOS MATRICULADOS</td>
            <td class="auto-style5" style="background-color: #CCCCCC; font-weight: bold;">N° ALUMNOS EGRESADOS</td>
            <td class="auto-style6" style="background-color: #CCCCCC; font-weight: bold;">N° ALUMNOS GRADUADOS</td>
        </tr>
        <tr>
            <td class="auto-style2">2020-1</td>
            <td class="auto-style2">500</td>
            <td class="auto-style3">11</td>
            <td>14</td>
        </tr>
        <tr>
            <td class="auto-style2">2020-2</td>
            <td class="auto-style2">529</td>
            <td class="auto-style3">7</td>
            <td>8</td>
        </tr>
    </table>
    <br />
    <p class="auto-style7" style="font-size: x-large; color: #00FFFF; font-weight: bold;">
        <br />
        AÑO 2019</p>
    <table class="auto-style1" style="border-style: groove; text-align: center;">
        <tr>
            <td class="auto-style4" style="background-color: #CCCCCC; font-weight: bold;">SEMESTRE ACADÉMICO</td>
            <td class="auto-style4" style="background-color: #CCCCCC; font-weight: bold;">N° ALUMNOS MATRICULADOS</td>
            <td class="auto-style5" style="background-color: #CCCCCC; font-weight: bold;">N° ALUMNOS EGRESADOS</td>
            <td class="auto-style6" style="background-color: #CCCCCC; font-weight: bold;">N° ALUMNOS GRADUADOS</td>
        </tr>
        <tr>
            <td class="auto-style2">2019-1</td>
            <td class="auto-style2">472</td>
            <td class="auto-style3">19</td>
            <td>25</td>
        </tr>
        <tr>
            <td class="auto-style2">2019-2</td>
            <td class="auto-style2">482</td>
            <td class="auto-style3">14</td>
            <td>7</td>
        </tr>
    </table>
    <br />
    <p class="auto-style7" style="font-size: x-large; color: #00FFFF; font-weight: bold;">
        <br />
        AÑO 2018</p>
    <table class="auto-style1" style="border-style: groove; text-align: center;">
        <tr>
            <td class="auto-style4" style="background-color: #CCCCCC; font-weight: bold;">SEMESTRE ACADÉMICO</td>
            <td class="auto-style4" style="background-color: #CCCCCC; font-weight: bold;">N° ALUMNOS MATRICULADOS</td>
            <td class="auto-style5" style="background-color: #CCCCCC; font-weight: bold;">N° ALUMNOS EGRESADOS</td>
            <td class="auto-style6" style="background-color: #CCCCCC; font-weight: bold;">N° ALUMNOS GRADUADOS</td>
        </tr>
        <tr>
            <td class="auto-style2">2018-1</td>
            <td class="auto-style2">465</td>
            <td class="auto-style3">12</td>
            <td>11</td>
        </tr>
        <tr>
            <td class="auto-style2">2018-2</td>
            <td class="auto-style2">448</td>
            <td class="auto-style3">17</td>
            <td>20</td>
        </tr>
    </table>
    <br />
    <p class="auto-style7" style="font-size: x-large; color: #00FFFF; font-weight: bold;">
        <br />
        AÑO 2017</p>
    <table class="auto-style1" style="border-style: groove; text-align: center;">
        <tr>
            <td class="auto-style4" style="background-color: #CCCCCC; font-weight: bold;">SEMESTRE ACADÉMICO</td>
            <td class="auto-style4" style="background-color: #CCCCCC; font-weight: bold;">N° ALUMNOS MATRICULADOS</td>
            <td class="auto-style5" style="background-color: #CCCCCC; font-weight: bold;">N° ALUMNOS EGRESADOS</td>
            <td class="auto-style6" style="background-color: #CCCCCC; font-weight: bold;">N° ALUMNOS GRADUADOS</td>
        </tr>
        <tr>
            <td class="auto-style2">2017-1</td>
            <td class="auto-style2">451</td>
            <td class="auto-style3">28</td>
            <td>30</td>
        </tr>
        <tr>
            <td class="auto-style2">2017-2</td>
            <td class="auto-style2">432</td>
            <td class="auto-style3">20</td>
            <td>26</td>
        </tr>
    </table>
    <br />
    <p class="auto-style7" style="font-size: x-large; color: #00FFFF; font-weight: bold;">
        <br />
        AÑO 2016</p>
    <table class="auto-style1" style="border-style: groove; text-align: center;">
        <tr>
            <td class="auto-style4" style="background-color: #CCCCCC; font-weight: bold;">SEMESTRE ACADÉMICO</td>
            <td class="auto-style4" style="background-color: #CCCCCC; font-weight: bold;">N° ALUMNOS MATRICULADOS</td>
            <td class="auto-style5" style="background-color: #CCCCCC; font-weight: bold;">N° ALUMNOS EGRESADOS</td>
            <td class="auto-style6" style="background-color: #CCCCCC; font-weight: bold;">N° ALUMNOS GRADUADOS</td>
        </tr>
        <tr>
            <td class="auto-style2">2016-1</td>
            <td class="auto-style2">462</td>
            <td class="auto-style3">26</td>
            <td>17</td>
        </tr>
        <tr>
            <td class="auto-style2">2016-2</td>
            <td class="auto-style2">424</td>
            <td class="auto-style3">23</td>
            <td>40</td>
        </tr>
    </table>
    <br />
        <div class="spacing_size spacing_size-initial" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; height: 16px;">
        </div>
    <div id="iguru_message_604f5c6469415" class="iguru_module_message_box type_custom" style="box-sizing: border-box; vertical-align: baseline; margin: 0px 0px 15px; border: 0px; padding: 12px 12px 12px 85px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; position: relative; z-index: 0; min-height: 73px; border-radius: 5px; box-shadow: rgba(0, 0, 0, 0.08) 6px 5px 25px; background-color: rgb(255, 255, 255); color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        <div class="message_icon_wrap" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: absolute; z-index: 1; top: 5px; left: 5px; width: 63px; height: calc(100% - 10px); border-radius: 5px; background-color: rgb(0, 206, 254);">
            <i class="message_icon fa fa-envelope-open" style="box-sizing: border-box; font: 35px / 1 FontAwesome; display: flex; text-rendering: auto; -webkit-font-smoothing: antialiased; -webkit-box-pack: center; justify-content: center; -webkit-box-align: center; align-items: center; height: 63px; text-align: center; color: rgb(255, 255, 255);"></i>
        </div>
        <div class="message_content" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
            <h4 class="message_title" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: Muli; font-size: 18px; font-weight: 700; font-style: inherit; color: rgb(0, 206, 254); line-height: 1.25;">INFORMES:</h4>
            <div class="message_text" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 8px 0px 0px; outline: 0px; font-family: inherit; font-size: 14px; font-weight: inherit; font-style: inherit; line-height: 1.25; color: rgb(95, 95, 95);">
                ep_sistemas@uandina.edu.pe</div>
        </div>
    </div>
    <div id="iguru_message_604f5c646949c" class="iguru_module_message_box type_custom" style="box-sizing: border-box; vertical-align: baseline; margin: 0px 0px 15px; border: 0px; padding: 12px 12px 12px 85px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; position: relative; z-index: 0; min-height: 73px; border-radius: 5px; box-shadow: rgba(0, 0, 0, 0.08) 6px 5px 25px; background-color: rgb(255, 255, 255); color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        <div class="message_icon_wrap" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: absolute; z-index: 1; top: 5px; left: 5px; width: 63px; height: calc(100% - 10px); border-radius: 5px; background-color: rgb(0, 206, 254);">
            <i class="message_icon fa fa-phone" style="box-sizing: border-box; font: 35px / 1 FontAwesome; display: flex; text-rendering: auto; -webkit-font-smoothing: antialiased; -webkit-box-pack: center; justify-content: center; -webkit-box-align: center; align-items: center; height: 63px; text-align: center; color: rgb(255, 255, 255);"></i>
        </div>
        <div class="message_content" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
            <h4 class="message_title" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: Muli; font-size: 18px; font-weight: 700; font-style: inherit; color: rgb(0, 206, 254); line-height: 1.25;">CONTACTO:</h4>
            <div class="message_text" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 8px 0px 0px; outline: 0px; font-family: inherit; font-size: 14px; font-weight: inherit; font-style: inherit; line-height: 1.25; color: rgb(95, 95, 95);">
                084 - 60 5000 / ANEXO: 1404</div>
        </div>
    </div>
    </asp:Content>
