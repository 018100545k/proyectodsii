﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Mallacur.aspx.cs" Inherits="SistemaAcademicoUniversidad.Mallacur" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="message_content" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
        <h4 class="message_title" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: Muli; font-size: 18px; font-weight: 700; font-style: inherit; color: rgb(0, 206, 254); line-height: 1.25;">&nbsp;</h4>
        <h4 class="message_title" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: Muli; font-size: 18px; font-weight: 700; font-style: inherit; color: rgb(0, 206, 254); line-height: 1.25;">DURACIÓN</h4>
        <div class="message_text" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 8px 0px 0px; outline: 0px; font-family: inherit; font-size: 14px; font-weight: inherit; font-style: inherit; line-height: 1.25; color: rgb(95, 95, 95);">
            10 ciclos académicos<br />
            <br />
            <div class="wpb_column vc_column_container vc_col-sm-4" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; width: 400px; position: relative; min-height: 1px; float: left;">
                <div class="vc_column-inner" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 35px 15px 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; width: 400px;">
                    <div class="wpb_wrapper" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                        <div id="iguru_dlh_604f50bc1ab16" class="iguru_module_double_headings aleft " style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: Muli; font-size: 16px; font-weight: inherit; font-style: inherit; text-align: left;">
                            <div class="dlh_subtitle" style="box-sizing: border-box; vertical-align: baseline; margin: 0px 0px 12px; border: 0px; padding: 0px; outline: 0px; font-family: Quicksand; font-size: 16px; font-weight: 700; font-style: inherit; display: inline-block; position: relative; z-index: 1; color: rgb(0, 206, 254); line-height: 20px;">
                                <span style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: inherit; font-weight: inherit; font-style: inherit; line-height: inherit; color: inherit;">
                                <br class="Apple-interchange-newline" />
                                CERTIFICACIONES INTERMEDIAS</span></div>
                        </div>
                        <div class="iguru_module_spacing" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                            <div class="spacing_size spacing_size-initial" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; height: 16px;">
                            </div>
                        </div>
                        <div class="wpb_text_column wpb_content_element " style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                            <div class="wpb_wrapper" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                                <ul class="iguru_dot secondary" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 20px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                                    <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">6to semestre: Analista de Sistemas.</li>
                                    <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">9no semestre: Administrador de Redes.</li>
                                </ul>
                            </div>
                        </div>
                        <div id="iguru_spacer_604f50bc1abca" class="iguru_module_spacing responsive_active" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                            <div class="spacing_size spacing_size-initial" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; display: block; height: 30px;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column vc_column_container vc_col-sm-4" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; width: 400px; position: relative; min-height: 1px; float: left;">
                <div class="vc_column-inner" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 35px 15px 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; width: 400px;">
                    <div class="wpb_wrapper" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                        <div id="iguru_spacer_604f50bc1ac30" class="iguru_module_spacing responsive_active" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                            <div class="spacing_size spacing_size-initial" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; display: block; height: 10px;">
                            </div>
                        </div>
                        <div id="iguru_dlh_604f50bc1ac4f" class="iguru_module_double_headings aleft " style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: Muli; font-size: 16px; font-weight: inherit; font-style: inherit; text-align: left;">
                            <div class="dlh_subtitle" style="box-sizing: border-box; vertical-align: baseline; margin: 0px 0px 12px; border: 0px; padding: 0px; outline: 0px; font-family: Quicksand; font-size: 16px; font-weight: 700; font-style: inherit; display: inline-block; position: relative; z-index: 1; color: rgb(0, 206, 254); line-height: 20px;">
                                <span style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: inherit; font-weight: inherit; font-style: inherit; line-height: inherit; color: inherit;">GRADO ACADÉMICO</span></div>
                        </div>
                        <div class="iguru_module_spacing" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                            <div class="spacing_size spacing_size-initial" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; height: 16px;">
                            </div>
                        </div>
                        <div class="wpb_text_column wpb_content_element " style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                            <div class="wpb_wrapper" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                                <ul class="iguru_dot secondary" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 20px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                                    <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">Bachiller en Ingeniería de Sistemas</li>
                                </ul>
                            </div>
                        </div>
                        <div id="iguru_spacer_604f50bc1ace6" class="iguru_module_spacing responsive_active" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                            <div class="spacing_size spacing_size-initial" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; display: block; height: 30px;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column vc_column_container vc_col-sm-4" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; width: 400px; position: relative; min-height: 1px; float: left;">
                <div class="vc_column-inner" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 35px 15px 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; width: 400px;">
                    <div class="wpb_wrapper" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                        <div id="iguru_spacer_604f50bc1ad47" class="iguru_module_spacing responsive_active" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(245, 247, 248); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                            <div class="spacing_size spacing_size-initial" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; display: block; height: 10px;">
                            </div>
                        </div>
                        <div id="iguru_dlh_604f50bc1ad65" class="iguru_module_double_headings aleft " style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: Muli; font-size: 16px; font-weight: 400; font-style: normal; text-align: left; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(245, 247, 248); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                            <div class="dlh_subtitle" style="box-sizing: border-box; vertical-align: baseline; margin: 0px 0px 12px; border: 0px; padding: 0px; outline: 0px; font-family: Quicksand; font-size: 16px; font-weight: 700; font-style: inherit; display: inline-block; position: relative; z-index: 1; color: rgb(0, 206, 254); line-height: 20px;">
                                <span style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: inherit; font-weight: inherit; font-style: inherit; line-height: inherit; color: inherit;">TÍTULO PROFESIONAL</span></div>
                        </div>
                        <div class="iguru_module_spacing" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(245, 247, 248); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                            <div class="spacing_size spacing_size-initial" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; height: 16px;">
                            </div>
                        </div>
                        <div class="wpb_text_column wpb_content_element " style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(245, 247, 248); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                            <div class="wpb_wrapper" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                                <ul class="iguru_dot secondary" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 20px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                                    <li style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px 0px 0px calc(0.5em + 14px); outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: relative; z-index: 1; line-height: 40px; list-style: none;">Ingeniero de Sistemas</li>
                                </ul>
                            </div>
                        </div>
                        <div id="iguru_spacer_604f50bc1adf8" class="iguru_module_spacing responsive_active" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(245, 247, 248); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                            <br class="Apple-interchange-newline" />
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <div id="iguru_dlh_604f50bc1aefa" class="iguru_module_double_headings aleft " style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: Muli; font-size: 16px; font-weight: 400; font-style: normal; text-align: left; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                <div class="dlh_subtitle" style="box-sizing: border-box; vertical-align: baseline; margin: 0px 0px 12px; border: 0px; padding: 0px; outline: 0px; font-family: Quicksand; font-size: 16px; font-weight: 700; font-style: inherit; display: inline-block; position: relative; z-index: 1; color: rgb(0, 206, 254); line-height: 20px;">
                    <span style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: inherit; font-weight: inherit; font-style: inherit; line-height: inherit; color: inherit;">
                    <br class="Apple-interchange-newline" />
                    MALLA CURRICULAR</span></div>
                <div class="dlh_title" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 32px; font-weight: 900; font-style: inherit; position: relative; z-index: 1; color: rgb(44, 44, 44); line-height: 1.563;">
                    <span style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: inherit; font-weight: inherit; font-style: inherit; line-height: inherit; color: inherit;">2016</span></div>
            </div>
            <div id="iguru_spacer_604f50bc1af53" class="iguru_module_spacing responsive_active" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                <br class="Apple-interchange-newline" />
            </div>
            <img src="https://www.uandina.edu.pe/wp-content/uploads/2021/02/IS-malla-curricular-2016.jpg" /><br />
            <br />
            <div id="iguru_spacer_604f5c6465396" class="iguru_module_spacing responsive_active" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                <br class="Apple-interchange-newline" />
                <div class="spacing_size spacing_size-initial" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; display: block; height: 10px;">
                </div>
            </div>
            <div class="wpb_text_column wpb_content_element  vc_custom_1613194033303" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; background-color: rgb(245, 247, 248) !important; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                <div class="wpb_wrapper" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                    <p style="box-sizing: border-box; vertical-align: baseline; margin: 0px 0px 15px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; color: rgb(94, 106, 120); letter-spacing: -0.1px; text-align: justify;">
                        <a href="https://www.uandina.edu.pe/descargas/documentos/dda/ing-sistemas-PE-2016.pdf" rel="noopener noreferrer" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; transition: color 0.3s ease 0s; text-decoration: none; color: rgb(29, 192, 222);" target="_blank">
                        <img class="alignleft size-full" src="https://www.uandina.edu.pe/descargas/iconos/icon-malla-curricular.png" style="box-sizing: border-box; vertical-align: top; margin: 10px 20px 10px 10px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; max-width: 100%; height: auto; user-select: none; float: left;" /></a></p>
                    <h2 style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 20px 0px 0px; outline: 0px; font-family: Muli; font-size: 18px; font-weight: 900; font-style: inherit; color: rgb(44, 44, 44); line-height: 52px;">DESCARGAR PLAN DE ESTUDIOS 2016</h2>
                </div>
            </div>
            <br />
            <br />
            <br />
            <div id="iguru_dlh_604f5c646550c" class="iguru_module_double_headings aleft " style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: Muli; font-size: 16px; font-weight: 400; font-style: normal; text-align: left; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                <div class="dlh_subtitle" style="box-sizing: border-box; vertical-align: baseline; margin: 0px 0px 12px; border: 0px; padding: 0px; outline: 0px; font-family: Quicksand; font-size: 16px; font-weight: 700; font-style: inherit; display: inline-block; position: relative; z-index: 1; color: rgb(0, 206, 254); line-height: 20px;">
                    <span style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: inherit; font-weight: inherit; font-style: inherit; line-height: inherit; color: inherit;">
                    <br class="Apple-interchange-newline" />
                    MALLA CURRICULAR</span></div>
                <div class="dlh_title" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 32px; font-weight: 900; font-style: inherit; position: relative; z-index: 1; color: rgb(44, 44, 44); line-height: 1.563;">
                    <span style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: inherit; font-weight: inherit; font-style: inherit; line-height: inherit; color: inherit;">2020</span></div>
            </div>
            <div id="iguru_spacer_604f5c6465571" class="iguru_module_spacing responsive_active" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                <br class="Apple-interchange-newline" />
            </div>
            <img src="https://www.uandina.edu.pe/wp-content/uploads/2021/02/IS-malla-curricular-2020.png" /><br />
            <br />
            <br />
            <div id="iguru_spacer_604f5c6465c02" class="iguru_module_spacing responsive_active" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                <br class="Apple-interchange-newline" />
                <div class="spacing_size spacing_size-initial" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; display: block; height: 10px;">
                </div>
            </div>
            <div class="wpb_text_column wpb_content_element  vc_custom_1613194504066" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; background-color: rgb(245, 247, 248) !important; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                <div class="wpb_wrapper" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                    <p style="box-sizing: border-box; vertical-align: baseline; margin: 0px 0px 15px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; color: rgb(94, 106, 120); letter-spacing: -0.1px; text-align: justify;">
                        <a href="https://www.uandina.edu.pe/descargas/documentos/dda/ing-sistemas-PE-2020.pdf" rel="noopener noreferrer" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; transition: color 0.3s ease 0s; text-decoration: none; color: rgb(29, 192, 222);" target="_blank">
                        <img class="alignleft size-full" src="https://www.uandina.edu.pe/descargas/iconos/icon-malla-curricular.png" style="box-sizing: border-box; vertical-align: top; margin: 10px 20px 10px 10px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; max-width: 100%; height: auto; user-select: none; float: left;" /></a></p>
                    <h2 style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 20px 0px 0px; outline: 0px; font-family: Muli; font-size: 18px; font-weight: 900; font-style: inherit; color: rgb(44, 44, 44); line-height: 52px;">DESCARGAR PLAN DE ESTUDIOS 2020</h2>
                </div>
            </div>
            <div id="iguru_spacer_604f5c6465c75" class="iguru_module_spacing responsive_active" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                <br class="Apple-interchange-newline" />
            </div>
            <div id="iguru_message_604f5c6469415" class="iguru_module_message_box type_custom" style="box-sizing: border-box; vertical-align: baseline; margin: 0px 0px 15px; border: 0px; padding: 12px 12px 12px 85px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; position: relative; z-index: 0; min-height: 73px; border-radius: 5px; box-shadow: rgba(0, 0, 0, 0.08) 6px 5px 25px; background-color: rgb(255, 255, 255); color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                <div class="message_icon_wrap" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: absolute; z-index: 1; top: 5px; left: 5px; width: 63px; height: calc(100% - 10px); border-radius: 5px; background-color: rgb(0, 206, 254);">
                    <br class="Apple-interchange-newline" />
                    <i class="message_icon fa fa-envelope-open" style="box-sizing: border-box; font: 35px / 1 FontAwesome; display: flex; text-rendering: auto; -webkit-font-smoothing: antialiased; -webkit-box-pack: center; justify-content: center; -webkit-box-align: center; align-items: center; height: 63px; text-align: center; color: rgb(255, 255, 255);"></i>
                </div>
                <div class="message_content" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                    <h4 class="message_title" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: Muli; font-size: 18px; font-weight: 700; font-style: inherit; color: rgb(0, 206, 254); line-height: 1.25;">INFORMES:</h4>
                    <div class="message_text" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 8px 0px 0px; outline: 0px; font-family: inherit; font-size: 14px; font-weight: inherit; font-style: inherit; line-height: 1.25; color: rgb(95, 95, 95);">
                        ep_sistemas@uandina.edu.pe</div>
                </div>
            </div>
            <div id="iguru_message_604f5c646949c" class="iguru_module_message_box type_custom" style="box-sizing: border-box; vertical-align: baseline; margin: 0px 0px 15px; border: 0px; padding: 12px 12px 12px 85px; outline: 0px; font-family: &quot;Open Sans&quot;; font-size: 16px; font-weight: 400; font-style: normal; position: relative; z-index: 0; min-height: 73px; border-radius: 5px; box-shadow: rgba(0, 0, 0, 0.08) 6px 5px 25px; background-color: rgb(255, 255, 255); color: rgb(95, 95, 95); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">
                <div class="message_icon_wrap" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit; position: absolute; z-index: 1; top: 5px; left: 5px; width: 63px; height: calc(100% - 10px); border-radius: 5px; background-color: rgb(0, 206, 254);">
                    <i class="message_icon fa fa-phone" style="box-sizing: border-box; font: 35px / 1 FontAwesome; display: flex; text-rendering: auto; -webkit-font-smoothing: antialiased; -webkit-box-pack: center; justify-content: center; -webkit-box-align: center; align-items: center; height: 63px; text-align: center; color: rgb(255, 255, 255);"></i>
                </div>
                <div class="message_content" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: inherit; font-size: 16px; font-weight: inherit; font-style: inherit;">
                    <h4 class="message_title" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 0px; outline: 0px; font-family: Muli; font-size: 18px; font-weight: 700; font-style: inherit; color: rgb(0, 206, 254); line-height: 1.25;">CONTACTO:</h4>
                    <div class="message_text" style="box-sizing: border-box; vertical-align: baseline; margin: 0px; border: 0px; padding: 8px 0px 0px; outline: 0px; font-family: inherit; font-size: 14px; font-weight: inherit; font-style: inherit; line-height: 1.25; color: rgb(95, 95, 95);">
                        084 - 60 5000 / ANEXO: 1404</div>
                </div>
            </div>
            <br />
        </div>
    </div>
</asp:Content>
