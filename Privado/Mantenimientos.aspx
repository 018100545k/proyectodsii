﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Mantenimientos.aspx.cs" Inherits="SistemaAcademicoUniversidad.Privado.TAlumno" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    </head>
<body>
    <form id="form1" runat="server">
        <div>
            <p style="font-size: x-large; font-weight: bold">
                &nbsp;</p>
            <p style="font-size: x-large; font-weight: bold">
        Mantenimiento Alumno</p>
    <p>
    CodAlumno:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:TextBox ID="txtcodalumnoalu" runat="server" />
</p>
<p>
    APaterno:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
    <asp:TextBox ID="txtapaternoalu" runat="server" />
</p>
<p>
    AMaterno:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:TextBox ID="txtamaternoalu" runat="server" />
</p>
<p>
    Nombres:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
    <asp:TextBox ID="txtnombresalu" runat="server" />
</p>
<p>
    Usuario:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:TextBox ID="txtusuarioalu" runat="server" />
</p>
<p>
    CodCarrera:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;
    <asp:TextBox ID="txtcodcarreraalu" runat="server" />
</p>
<p>
    <asp:Button ID="btnAgregaralu" runat="server" OnClick="btnAgregar_Click" Text="Agregar" />
    <asp:Button ID="btnEliminaralu" runat="server" OnClick="btnEliminar_Click1" style="height: 26px" Text="Eliminar" />
    <asp:Button ID="btnActualizaralu" runat="server" OnClick="btnActualizar_Click" Text="Actualizar" />
</p>
<p>
    <asp:GridView runat="server" ID="gvEjemploalumno" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black">
        <FooterStyle BackColor="#CCCCCC" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
        <RowStyle BackColor="White" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#808080" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#383838" />
    </asp:GridView>
    </p>
            <p>
                &nbsp;</p>
            <p style="font-size: x-large; font-weight: bold">
        Mantenimiento Docente</p>
    <p>
    CodDocente:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtcoddocentedo" runat="server" />
    </p>
    <p>
    APaterno:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
        <asp:TextBox ID="txtapaternodo" runat="server" />
    </p>
    <p>
    AMaterno:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtamaternodo" runat="server" />
    </p>
    <p>
    Nombres:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtnombresdo" runat="server" />
    </p>
    <p>
    Usuario:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtusuariodo" runat="server" />
    </p>
    <p>
        <asp:Button ID="btnAgregardo" runat="server" OnClick="btnAgregar0_Click" Text="Agregar" />
        <asp:Button ID="btnEliminardo" runat="server" OnClick="btnEliminar0_Click1" style="height: 26px" Text="Eliminar" />
        <asp:Button ID="btnActualizardo" runat="server" OnClick="btnActualizar0_Click" Text="Actualizar" />
    </p>
    <asp:GridView runat="server" ID="gvEjemplodocente" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black">
        <FooterStyle BackColor="#CCCCCC" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
        <RowStyle BackColor="White" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#808080" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#383838" />
    </asp:GridView>
            <p>
                &nbsp;</p>
            <p style="font-size: x-large; font-weight: bold">
        Mantenimiento Asignatura</p>
    <p>
    CodAsignatura:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtcodasignatura" runat="server" />
    </p>
    <p>
    Asignatura:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
        <asp:TextBox ID="txtasignatura" runat="server" />
    </p>
    <p>
        CodRequisito:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtcodrequisito" runat="server" />
    </p>
    <p>
        <asp:Button ID="btnAgregarasig" runat="server" OnClick="btnAgregar1_Click" Text="Agregar" />
        <asp:Button ID="Button1" runat="server" Height="26px" OnClick="Button1_Click" Text="Eliminar" Width="67px" />
        <asp:Button ID="Button2" runat="server" Height="26px" OnClick="Button2_Click" Text="Actualizar" Width="88px" />
    </p>
    <asp:GridView runat="server" ID="gvEjemploasignatura" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black">
        <FooterStyle BackColor="#CCCCCC" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
        <RowStyle BackColor="White" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#808080" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#383838" />
    </asp:GridView>
            <p>
                &nbsp;</p>

        </div>
    </form>
</body>
</html>
