﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TAsignatura.aspx.cs" Inherits="SistemaAcademicoUniversidad.Privado.TAsignatura" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    </head>
<body>
    <form id="form1" runat="server">
        <div>
            <p style="font-size: x-large; font-weight: bold">
                &nbsp;</p>
            <p style="font-size: x-large; font-weight: bold">
        Mantenimiento Asignatura</p>
    <p>
    CodAsignatura:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtcodasignatura" runat="server" />
    </p>
    <p>
    Asignatura:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
        <asp:TextBox ID="txtasignatura" runat="server" />
    </p>
    <p>
        CodRequisito:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtcodrequisito" runat="server" />
    </p>
    <p>
        <asp:Button ID="btnAgregar1" runat="server" OnClick="btnAgregar1_Click" Text="Agregar" />
        <asp:Button ID="btnEliminar1" runat="server" OnClick="btnEliminar_Click1" style="height: 26px" Text="Eliminar" />
        <asp:Button ID="btnActualizar1" runat="server" OnClick="btnActualizar_Click" Text="Actualizar" />
    </p>
    <asp:GridView runat="server" ID="gvEjemplo1" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black">
        <FooterStyle BackColor="#CCCCCC" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
        <RowStyle BackColor="White" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#808080" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#383838" />
    </asp:GridView>
        </div>
    </form>
</body>
</html>
