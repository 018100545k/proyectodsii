﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TDocente.aspx.cs" Inherits="SistemaAcademicoUniversidad.Privado.TDocente" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    </head>
<body>
    <form id="form1" runat="server">
        <div>
            <p style="font-size: x-large; font-weight: bold">
                &nbsp;</p>
            <p style="font-size: x-large; font-weight: bold">
                &nbsp;</p>
            <p style="font-size: x-large; font-weight: bold">
        Mantenimiento Docente</p>
    <p>
    CodDocente:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtcoddocente" runat="server" />
    </p>
    <p>
    APaterno:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
        <asp:TextBox ID="txtapaterno" runat="server" />
    </p>
    <p>
    AMaterno:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtamaterno" runat="server" />
    </p>
    <p>
    Nombres:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtnombres" runat="server" />
    </p>
    <p>
    Usuario:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtusuario" runat="server" />
    </p>
    <p>
        <asp:Button ID="btnAgregar0" runat="server" OnClick="btnAgregar0_Click" Text="Agregar" />
        <asp:Button ID="btnEliminar0" runat="server" OnClick="btnEliminar0_Click1" style="height: 26px" Text="Eliminar" />
        <asp:Button ID="btnActualizar0" runat="server" OnClick="btnActualizar0_Click" Text="Actualizar" />
    </p>
    <asp:GridView runat="server" ID="gvEjemplo" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black">
        <FooterStyle BackColor="#CCCCCC" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
        <RowStyle BackColor="White" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#808080" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#383838" />
    </asp:GridView>
        </div>
    </form>
</body>
</html>
